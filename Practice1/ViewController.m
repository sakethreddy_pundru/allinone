//
//  ViewController.m
//  Practice1
//
//  Created by admin on 18/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
#import "SignUpViewController.h"
#import "LoginViewController.h"
@import MobileCoreServices;

@interface ViewController (){
    UIImage *imageview;
    UIImagePickerController *imgpckr;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"1070.png"];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    imageView.frame = self.view.frame;
    [self.view addSubview:imageView];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(140, 300, 100, 100)];
    button.backgroundColor = [UIColor redColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"Click me..." forState:UIControlStateNormal];
     button.layer.cornerRadius = 50;
    [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    imgpckr = [[UIImagePickerController alloc]init];
    imgpckr.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imgpckr.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage, nil];
//    [self presentViewController:imgpckr animated:YES completion:nil];

    }

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    if ([[info valueForKey:UIImagePickerControllerMediaType]isEqualToString:@"public.image"]) {
        imageview = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:imageview];
    imageView.frame = CGRectMake(20, 50, 200, 200);
    [self.view addSubview:imageView];
    
    [imgpckr dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [imgpckr dismissViewControllerAnimated:YES completion:nil];
}


-(void)buttonClick{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Idiot" message:@"First Select Any..." preferredStyle:(UIAlertControllerStyleActionSheet)];
    
    UIAlertAction *signUp = [UIAlertAction actionWithTitle:@"Don't have an account...? Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        SignUpViewController *supvc = [[SignUpViewController alloc]init];
        [self.navigationController pushViewController:supvc animated:YES];
        
    }];
    
    UIAlertAction *login = [UIAlertAction actionWithTitle:@"Have an account!!! Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        LoginViewController *lvc = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:lvc animated:YES];
        
    }];
    
    UIAlertAction *browse = [UIAlertAction actionWithTitle:@"Browse" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [imgpckr dismissViewControllerAnimated:YES completion:nil];
        [self presentViewController:imgpckr animated:YES completion:nil];

        
    }];
    

    
    UIAlertAction *Cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    [alertController addAction:signUp];
    [alertController addAction:login];
    
    
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
    [alertController addAction:browse];
    [alertController addAction:Cancel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }

@end
