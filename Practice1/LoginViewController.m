//
//  LoginViewController.m
//  Practice1
//
//  Created by admin on 18/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "LoginViewController.h"
#import "SecondViewController.h"
#import "SignUpViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController{
    
    UITextField *username;
    UITextField *password;
    
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"1070.png"];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    imageView.frame = self.view.frame;
    [self.view addSubview:imageView];
    
    username = [[UITextField alloc]initWithFrame:CGRectMake(100, 200, 250, 25)];
    username.backgroundColor = [UIColor lightTextColor];
    username.placeholder = @"Username";
    username.textColor = [UIColor redColor];
    username.borderStyle =UITextBorderStyleNone;
    username.layer.cornerRadius = 5;
    [self.view addSubview:username];
    
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(100, 250, 250, 25)];
    password.backgroundColor = [UIColor lightTextColor];
    password.placeholder = @"Password";
    password.textColor = [UIColor redColor];
    password.borderStyle =UITextBorderStyleNone;
    password.layer.cornerRadius = 5;
    password.secureTextEntry = YES;
    password.delegate = self;
    [self.view addSubview:password];
    
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(140, 300, 80, 80)];
    button.backgroundColor = [UIColor redColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"Login" forState:UIControlStateNormal];
    button.layer.cornerRadius = 40;
    [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *button1 = [[UIButton alloc]initWithFrame:CGRectMake(25, 400, 450, 50)];
    [button1 setTitle:@"Don't have an account ?" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(button1Click) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button1];
    

}


-(void)buttonClick{
    NSString *lusername;
    NSString *lpassword;
    lusername = [[NSUserDefaults standardUserDefaults]valueForKey:@"Username"];
    lpassword = [[NSUserDefaults standardUserDefaults]valueForKey:@"Password"];
    if ([username.text isEqualToString:lusername]&&[password.text isEqualToString:lpassword]) {
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
        
    }else{
        UIAlertView *note2 = [[UIAlertView alloc]initWithTitle:@"oops" message:@"invalid credentials" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [note2 show];
        password.text = nil;
        
    }
    
}


-(void)button1Click{
    
    SignUpViewController *supvc = [[SignUpViewController alloc]init];
    [self.navigationController pushViewController:supvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
