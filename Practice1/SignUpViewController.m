//
//  SignUpViewController.m
//  Practice1
//
//  Created by admin on 18/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "SignUpViewController.h"
#import "LoginViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController{
    UITextField *username;
    UITextField *password;
    UITextField *reEnterPassword;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"1070.png"];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    imageView.frame = self.view.frame;
    [self.view addSubview:imageView];
    
    username = [[UITextField alloc]initWithFrame:CGRectMake(70, 200, 250, 25)];
    username.backgroundColor = [UIColor lightTextColor];
    username.placeholder = @"Username";
    username.textColor = [UIColor redColor];
    username.borderStyle =UITextBorderStyleNone;
    username.layer.cornerRadius = 5;
    [self.view addSubview:username];
    
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(70, 250, 250, 25)];
    password.backgroundColor = [UIColor lightTextColor];
    password.placeholder = @"Password";
    password.textColor = [UIColor redColor];
    password.borderStyle =UITextBorderStyleNone;
    password.layer.cornerRadius = 5;
    password.secureTextEntry = YES;
    [self.view addSubview:password];
    
    
    reEnterPassword = [[UITextField alloc]initWithFrame:CGRectMake(70, 300, 250, 25)];
    reEnterPassword.backgroundColor = [UIColor lightTextColor];
    reEnterPassword.placeholder = @"reEnterPassword";
    reEnterPassword.textColor = [UIColor redColor];
    reEnterPassword.borderStyle =UITextBorderStyleNone;
    reEnterPassword.layer.cornerRadius = 5;
    reEnterPassword.secureTextEntry = YES;
    [self.view addSubview:reEnterPassword];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(70, 350, 80, 80)];
    button.backgroundColor = [UIColor redColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"SignUp" forState:UIControlStateNormal];
    button.layer.cornerRadius = 40;
    [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *button1 = [[UIButton alloc]initWithFrame:CGRectMake(220, 350, 80, 80)];
    button1.backgroundColor = [UIColor redColor];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button1 setTitle:@"Login" forState:UIControlStateNormal];
    button1.layer.cornerRadius = 40;
    [button1 addTarget:self action:@selector(button1Click) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button1];

    }


-(void)buttonClick{
    
    [[NSUserDefaults standardUserDefaults]setValue:username.text forKey:@"Username"];
    [[NSUserDefaults standardUserDefaults]setValue:password.text forKey:@"Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if ([username.text isEqualToString:username.text]&&[password.text isEqualToString:reEnterPassword.text]) {
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Congratulations" message:@"Your Successfully Registered" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert1 show];
    } else {
        UIAlertView *alert2 = [[UIAlertView alloc]initWithTitle:@"Ooops!!!"  message:@"Check your Fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert2 show];
    }
}

-(void)button1Click{
    
    LoginViewController *lvc = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:lvc animated:YES];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
