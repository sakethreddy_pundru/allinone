//
//  SecondViewController.m
//  Practice1
//
//  Created by admin on 18/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController{
    
    NSMutableArray *mainarray;
}

    - (void)viewDidLoad {
        [super viewDidLoad];
        
        mainarray = [[NSMutableArray alloc]init];
        
        NSArray *innerarray1 = @[
                                 @{
                                     @"title":@"WiFi",
                                     @"image":@"ic_wifi_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Bluetooth",
                                     @"image":@"ic_settings_bluetooth_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"SIM Cards",
                                     @"image":@"ic_sim_card_black_24dp_1x"
                                     
                                     }
                                 ,@{
                                     @"title":@"Data Usage",
                                     @"image":@"ic_network_check_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"More",
                                     @"image":@"ic_more_black_24dp_1x"
                                     
                                     }
                                 ];
        
        NSArray *innerarray2 = @[
                                 @{
                                     @"title":@"Home",
                                     @"image":@"ic_home_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Display",
                                     @"image":@"ic_brightness_6_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Sound & Notification",
                                     @"image":@"ic_notifications_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Storage",
                                     @"image":@"ic_storage_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Battery",
                                     @"image":@"ic_battery_std_black_24dp_1x"
                                     }
                                 ];
        
        NSArray *innerarray3 = @[
                                 @{
                                     @"title":@"Location",
                                     @"image":@"ic_place_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Security",
                                     @"image":@"ic_security_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Accounts",
                                     @"image":@"ic_account_box_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Language & input",
                                     @"image":@"ic_language_black_24dp_1x"
                                     }
                                 ,@{
                                     @"title":@"Backup & Reset",
                                     @"image":@"ic_settings_backup_restore_black_24dp_1x"
                                     }
                                 ];
        
        [mainarray addObject:innerarray1];
        [mainarray addObject:innerarray2];
        [mainarray addObject:innerarray3];
        
        
        UITableView *tableview = [[UITableView alloc]init];
        tableview.frame = CGRectMake(0,50,500,1000);
        tableview.delegate = self;
        tableview.dataSource = self;
        [self.view addSubview:tableview];
        
    }
    
    
    
    -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return mainarray.count;
    }
    
    -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return [[mainarray objectAtIndex:section] count];
    }
    
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *STRING = @"cell";
    
    UILabel *label;
    UIImageView *imageview;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:STRING];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STRING];
        
        imageview=[[UIImageView alloc]initWithFrame:CGRectMake(30, 20, 20, 20)];
        [cell addSubview:imageview];
        
        label =[[UILabel alloc]init];
        label.frame = CGRectMake(75, 20, 150, 20);
        [cell addSubview:label];
        

        
    }
    NSArray *currentArray = [mainarray objectAtIndex:indexPath.section];
    NSDictionary *mdict = [currentArray objectAtIndex:indexPath.row];
    label.text = [mdict valueForKey:@"title"];
     imageview.image=[UIImage imageNamed:[mdict valueForKey:@"image"]];
    return cell;
}
    
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
        
        
        if (section==0) {
            return @"Wireless & Networks";
            
        }else if(section==1){
            return @"Device";
            
        }else{
            return @"Personal";
        }
    }
    
    
    - (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
    {
        
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        [header.textLabel setTextColor:[UIColor lightGrayColor]];
    }
    



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
